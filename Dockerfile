FROM ubuntu:14.04
MAINTAINER Fran Amado (MARKETEER)

# Actualizacion de los 'sources' a la ultima version
RUN apt-get update

# Instalar los paquetes del sistema necesarios para python
RUN apt-get install -qy python \
                        python-dev \
                        python-pip \
                        python-setuptools \
                        build-essential \
                        vim \
                        wget \
                        net-tools \
                        git \
                        nginx \
                        supervisor \
                        python-scipy \
                        lame \
                        python-lxml --fix-missing





###############################
#
#        Cherrypy
#
###############################

# Copiar la configuracion de nginx de la aplicion
RUN pip install CherryPy

COPY withcherrypy /withcherrypy
